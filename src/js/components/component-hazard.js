$('.js-hazard-checkbox').each(function() {
    let checkbox =  $(this),
        id =        checkbox.attr('data-hazard'),
        details =   $('.js-hazard-details[data-hazard="' + id + '"]');

    checkbox.off('change.hazard').on('change.hazard', function() {
        if (checkbox.is(':checked')) {
            details.removeClass('hidden');
        } else {
            details.addClass('hidden');
        }
    });
});