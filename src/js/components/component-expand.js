$('.js-expander').each(function() {
    let button =    $(this),
        name =      button.attr('data-expand'),
        target =    $('.js-expanded[data-expand="' + name + '"]');

    button.off('click.expand').on('click.expand', function() {
        button.toggleClass('expanded');
        target.toggleClass('expanded');
    });
});