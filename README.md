## Perfectorium ##

#### 1. Setup Instructions:

   a. Clone
   ```
   git clone git@bitbucket.org:kamlyuk/perfectorium.git
   ```

   b. Install dependencies
   ```
   npm i
   ```
   b.2. Install Gulp globally if it's not installed
   ```
   npm i gulp-cli -g
   ```

   c. Build project
   ```
   gulp